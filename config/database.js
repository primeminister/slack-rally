"use strict";
const config = require("config");

module.exports = {
  "development": {
    "storage": `${config.Database.path}/${config.Database.file}`,
    "dialect": "sqlite"
  },
  "TEST": {
    "storage": `${config.Database.path}/${config.Database.file}`,
    "dialect": "sqlite"
  },
  "PROD": {
    "storage": `${config.Database.path}/${config.Database.file}`,
    "dialect": "sqlite"
  }
}
