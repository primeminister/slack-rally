# README

This node app is an integration between Slack and CA Rally

## What is this repository for?

- If you use [Rally](https://www.broadcom.com/products/software/agile-development/rally-software)
- and if you use [Slack](https://slack.com/)

## How do I get set up?

- Get an API key for [Rally API key](https://rally1.rallydev.com/login/accounts/index.html#/keys)
- Find out your workspace ID from Rally
- Add Rally app to your Slack workspace: [Authenticate Rally app](https://slack.com/oauth/v2/authorize?client_id=2233728896.1026535827078&scope=app_mentions:read,channels:history,channels:read,chat:write,chat:write.customize,commands,groups:history,im:history,im:read,im:write,mpim:history,team:read,users.profile:read,users:read,users:read.email,channels:manage,groups:write,mpim:write,mpim:read)
- It will send you a message in Slack after athentication with a configure button. Please fill in the details in the modal:
  - RALLY_APIKEY is your connection to Rally: `export RALLY_APIKEY='_zjagd37dghwygh...'`.
  - RALLY_WORKSPACE_ID is your workspace ID: `export RALLY_WORKSPACE_ID='2176234322384'`.
- Invite the Rally app in your channel(s)

## Usage

- `/Rally US76523` Gives you the details of that specific userstory or defect
- `/Rally my` Gives you your stories or defects in a list
- `/Rally sprint ProjectName` Gives you all the issues in the current iteration in that specific project
- It listens to your conversations and when a rally issue is mentioned it retrieves the information and sends it back to you

## Deploying new code

`npm version [major | minor | path] -m "Release note..."`
After this the 'postversion' script will run and push the tag to origin. This will trigger a deployment pipeline in bitbucket.

## Feedback?

Do you have any feedback? Create an issue [here](https://bitbucket.org/primeminister/slack-rally/issues)