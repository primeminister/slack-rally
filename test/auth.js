process.env.NODE_ENV = "TEST";

const express = require("../built/server"),
  chai = require("chai"),
  chaiHttp = require("chai-http"),
  sinon = require("sinon"),
  expect = chai.expect,
  should = chai.should();

chai.use(chaiHttp);
chai.use(require("chai-as-promised"));

let server;

let mocks = {
  slack_after_oauthv2access: {
    "data": {
      "ok": true,
      "app_id": "A010SFRQB2A",
      "authed_user": {
        "id": "U02LTE7JT"
      },
      "scope": "channels:history,chat:write,chat:write.customize,commands,groups:history,im:history,mpim:history,users.profile:read,users:read,users:read.email,team:read",
      "token_type": "bot",
      "access_token": "xoxb-2233728896-1024504333780-5T2dFKJmcW3EOkeiOTjYI9bw",
      "bot_user_id": "U010QEU9TNY",
      "team": {
        "id": "T026VMESC",
        "name": "MoWD"
      },
      "enterprise": null,
      "warning": "superfluous_charset",
      "response_metadata": {
        "warnings": [
          "superfluous_charset"
        ]
      }
    }
  }
}

describe.skip("Slack auth tests", function() {
  before("start http server", function(done) {
    express.startServer().then(function(srv) {
      server = srv;
      done();
    });
  });
  after("close http server", function(done) {
    express.closeServer(server).then(function() {
      done();
    });
  });
  afterEach("restore stubs", function () {
    sinon.restore();
  });
  it("URL verification", function testPath(done) {
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_after_oauthv2access)
      .end((err, res) => {
        // console.dir(res);
        res.should.have.status(200);
        res.text.should.be.a("string");
        res.text.should.be.eql(mocks.slack_url_verification.challenge);
        done();
      });
  });
});