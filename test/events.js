process.env.NODE_ENV = "TEST";

const express = require("../built/server"),
  chai = require("chai"),
  chaiHttp = require("chai-http"),
  sinon = require("sinon"),
  expect = chai.expect,
  should = chai.should(),
  mocks = require("./mock/events");

chai.use(chaiHttp);
chai.use(require("chai-as-promised"));

let server;

describe("Slack event tests", function() {
  before("start http server", function(done) {
    express.startServer().then(function(srv) {
      server = srv;
      done();
    });
  });
  after("close http server", function(done) {
    express.closeServer(server).then(function() {
      done();
    });
  });
  afterEach("restore stubs", function () {
    sinon.restore();
  });
  it("URL verification", function testPath(done) {
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_url_verification)
      .end((err, res) => {
        // console.dir(res);
        res.should.have.status(200);
        res.text.should.be.a("string");
        res.text.should.be.eql(mocks.slack_url_verification.challenge);
        done();
      });
  });
  it("respond to all other message types then event_callback", function testPath(done) {
    sinon.stub(mocks.slack_event, 'type').value('foo_bar');
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event)
      .end((err, res) => {
        res.should.have.status(200);
        res.text.should.be.eql("");
        done();
      });
  });
  it("respond to event_callback with no issue", function testPath(done) {
    sinon.stub(mocks.slack_event.event, 'text').value('Just talking and no rally issues mentioned here');
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event)
      .end((err, res) => {
        res.should.have.status(200);
        res.text.should.be.eql("");
        done();
      });
  });
  it("respond to event_callback with issue at start", function testPath(done) {
    sinon.stub(mocks.slack_event.event, 'text').value('US42384 ga ik oppakken. Of kan ik iemand ergens anders mee helpen?');
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(1);
        done();
      });
  });
  it("respond to event_callback with issue at end", function testPath(done) {
    sinon.stub(mocks.slack_event.event, 'text').value('Ik kan iets oppakken of kan ik iemand ergens anders meehelpen? bv. US42384');
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(1);
        done();
      });
  });
  it("respond to event_callback with issue in middle", function testPath(done) {
    sinon.stub(mocks.slack_event.event, 'text').value('Ik kan US42384 oppakken of kan ik iemand ergens anders meehelpen?');
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(1);
        done();
      });
  });
  it("respond to event_callback with multiple issues", function testPath(done) {
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(2);
        done();
      });
  });
  it("respond to threaded events", function testPath(done) {
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event_thread)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(1);
        res.body.thread_ts.should.be.equal(mocks.slack_event_thread.event.thread_ts);
        done();
      });
  });
  it("respond to events with links", function testPath(done) {
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event_link)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.empty;
        done();
      });
  });
  it("respond to hidden events", function testPath(done) {
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event_hidden)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.empty;
        done();
      });
  });
  it("respond to bot events", function testPath(done) {
    chai
      .request(express.app)
      .post("/events")
      .send(mocks.slack_event_bot)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.empty;
        done();
      });
  });
});