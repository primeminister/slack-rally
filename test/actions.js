"use strict";

process.env.NODE_ENV = "TEST";

const Slack = require('../built/modules/slack'),
  express = require("../built/server"),
  chai = require("chai"),
  chaiHttp = require("chai-http"),
  sinon = require("sinon"),
  expect = chai.expect,
  should = chai.should(),
  mocks = require('./mock/actions');

chai.use(chaiHttp);
chai.use(require("chai-as-promised"));

function slackResponse() {
  return {
    'result': 'ok'
  };
}


let server;

describe.only("Slack actions tests", function() {
  let updateItemStub;

  before("start http server", function(done) {
    express.startServer().then(function(srv) {
      server = srv;
      done();
    });
  });
  after("close http server", function(done) {
    express.closeServer(server).then(function(){
      done();
    });
  });
  beforeEach("Wrap slack post request", function(){
    updateItemStub = sinon.stub(Slack, "doPostRequest");
  });
  afterEach("restore sinon", function () {
    sinon.restore();
    updateItemStub.restore();
  });
  it.only("Update Rally ScheduleState", function testBlock(done) {
    updateItemStub.returns(Promise.resolve());

    chai
      .request(express.app)
      .post("/actions")
      .send(mocks.slack_block_actions)
      .end((err, res) => {
        console.log(err);
        console.log(res);
        // res.should.have.status(200);
        assert.equal(updateItemStub.calledOnce, true);

        done();
      });
  });
  it("Trigger modal Rally config", function testBlock(done) {
    chai
      .request(express.app)
      .post("/actions")
      .send(mocks.slack_rally_config_submit)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
  it("Submit modal Rally config", function testBlock(done) {
    chai
      .request(express.app)
      .post("/actions")
      .send(mocks.slack_rally_config_submit)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
  it("Create Rally issue", function testBlock(done) {
    chai
      .request(express.app)
      .post("/actions")
      .send(mocks.slack_rally_config_submit)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });


});