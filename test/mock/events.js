// mock slack post
let slack_url_verification = {
  token: "Jhj5dZrVaK7ZwHHjRyZWjbDl",
  challenge: "3eZbrw1aBm2rZgRNFdxV2595E9CY3gmdALWMmHkvFXO7tYXAYM8P",
  type: "url_verification"
};
let slack_event = {
  token: "tuD33cR34MzPu21AhhDX6fAY",
  team_id: "T026VMESC",
  api_app_id: "A010SFRQB2A",
  event: {
    client_msg_id: "df2cc124-d99c-45e0-8c7e-71bb757ef83e",
    type: "message",
    text: "Kan ik US46075 oppakken of kan ik iemand ergens anders meehelpen? Of DE4056?",
    user: "U02LTE7JT",
    ts: "1585134517.001000",
    team: "T026VMESC",
    blocks: [],
    channel: "G010L1G2G4E",
    event_ts: "1585134517.001000",
    channel_type: "group"
  },
  type: "event_callback",
  event_id: "Ev010QHF0DS8",
  event_time: 1585134517,
  authed_users: ["U010QEU9TNY"]
};
let slack_event_thread = {
  "token": "tuD33cR34MzPu21AhhDX6fAY",
  "team_id": "T026VMESC",
  "api_app_id": "A010SFRQB2A",
  "event": {
    "client_msg_id": "e24b37c7-8c85-416b-a979-35dd44827a52",
    "type": "message",
    "text": "nog een keer F4000 dan",
    "user": "U02LTE7JT",
    "ts": "1585728744.000500",
    "team": "T026VMESC",
    "blocks": [],
    "thread_ts": "1585654548.003200",
    "parent_user_id": "U02LTE7JT",
    "channel": "G010L1G2G4E",
    "event_ts": "1585728744.000500",
    "channel_type": "group"
  },
  "type": "event_callback",
  "event_id": "Ev01163Y3GE8",
  "event_time": 1585728744,
  "authed_users": [
    "U010QEU9TNY"
  ]
};
let slack_event_link = {
  "token": "tuD33cR34MzPu21AhhDX6fAY",
  "team_id": "T026VMESC",
  "api_app_id": "A010SFRQB2A",
  "event": {
    "client_msg_id": "a5fd2429-2044-4ca3-b390-8813ec2d862a",
    "type": "message",
    "text": "<!channel> morgen om 09:00u is de terugkoppeling van de management review. Kunnen jullie ook <https://eur01.safelinks.protection.outlook.com/ap/t-59584e83/?url=https%3A%2F%2Fteams.microsoft.com%2Fl%2Fmeetup-join%2F19%253ab9008d11ee5d435684ce5bc9a8178267%2540thread.tacv2%2F1585227794026%3Fcontext%3D%257b%2522Tid%2522%253a%25220cf1e6d5-596c-4579-b6fe-bdae3a63c19f%2522%252c%2522Oid%2522%253a%2522aa008edf-38ed-4f9b-8ed1-7abc8934b091%2522%257d&amp;data=02%7C01%7C%7C684dfcf734734da41ad708d7d5806c24%7C0cf1e6d5596c4579b6febdae3a63c19f%7C0%7C0%7C637212620018901166&amp;sdata=dj1KRaJwA1ChTvHnrro374MLTewmRkfdshmQt%2Byy%2B0s%3D&amp;reserved=0|bijwonen> op deze <https://eur01.safelinks.protection.outlook.com/ap/t-59584e83/?url=https%3A%2F%2Fteams.microsoft.com%2Fl%2Fmeetup-join%2F19%253ab9008d11ee5d435684ce5bc9a8178267%2540thread.tacv2%2F1585227794026%3Fcontext%3D%257b%2522Tid%2522%253a%25220cf1e6d5-596c-4579-b6fe-bdae3a63c19f%2522%252c%2522Oid%2522%253a%2522aa008edf-38ed-4f9b-8ed1-7abc8934b091%2522%257d&amp;data=02%7C01%7C%7C684dfcf734734da41ad708d7d5806c24%7C0cf1e6d5596c4579b6febdae3a63c19f%7C0%7C0%7C637212620018901166&amp;sdata=dj1KRaJwA1ChTvHnrro374MLTewmRkfdshmQt%2Byy%2B0s%3D&amp;reserved=0|teams> meeting link",
    "user": "U02LTE7JT",
    "ts": "1585758145.002100",
    "team": "T026VMESC",
    "blocks": [],
    "channel": "G010L1G2G4E",
    "event_ts": "1585758145.002100",
    "channel_type": "group"
  },
  "type": "event_callback",
  "event_id": "Ev0119P3R2ES",
  "event_time": 1585758145,
  "authed_users": [
    "U010QEU9TNY"
  ]
};
let slack_event_link2 = {
  "token": "tuD33cR34MzPu21AhhDX6fAY",
  "team_id": "T026VMESC",
  "api_app_id": "A010SFRQB2A",
  "event": {
    "client_msg_id": "a5fd2429-2044-4ca3-b390-8813ec2d862a",
    "type": "message",
    "text": "https://pnl.gitlab.schubergphilis.com/BusinessServices/CIS/pnl-bas-cis-pbpr-consumer/-/merge_requests/7/diffs#8c1cb2d4a75307e3c8359d90f48a4caf8981b738",
    "user": "U02LTE7JT",
    "ts": "1585758145.002100",
    "team": "T026VMESC",
    "blocks": [],
    "channel": "G010L1G2G4E",
    "event_ts": "1585758145.002100",
    "channel_type": "group"
  },
  "type": "event_callback",
  "event_id": "Ev0119P3R2ES",
  "event_time": 1585758145,
  "authed_users": [
    "U010QEU9TNY"
  ]
};
let slack_event_hidden = {
  "token": "tuD33cR34MzPu21AhhDX6fAY",
  "team_id": "T026VMESC",
  "api_app_id": "A010SFRQB2A",
  "event": {
    "type": "message",
    "subtype": "message_deleted",
    "hidden": true,
    "deleted_ts": "1585759014.000001",
    "channel": "G010L1G2G4E",
    "event_ts": "1585759034.004100",
    "ts": "",
    "channel_type": "group"
  },
  "type": "event_callback",
  "event_id": "Ev0119QGQUKY",
  "event_time": 1585759034,
  "authed_users": [
    "U010QEU9TNY"
  ]
};

let slack_event_bot = {
  "token": "UgykeNntBHnBsvkj6toOZYPh",
  "team_id": "TCXK1L988",
  "api_app_id": "A0108LARNQH",
  "event": {
    "bot_id": "B010LVAFSFQ",
    "type": "message",
    "text": "Dummy test data about F4000 and others",
    "user": "U010LVAFUBG",
    "ts": "1585743189.044700",
    "team": "TCXK1L988",
    "bot_profile": {
      "id": "B010LVAFSFQ",
      "deleted": false,
      "name": "Rally",
      "updated": 1584959466,
      "app_id": "A0108LARNQH",
      "icons": {
        "image_36": "https://avatars.slack-edge.com/2020-03-23/1018755245648_e51bfdea5f55b401ada8_36.png",
        "image_48": "https://avatars.slack-edge.com/2020-03-23/1018755245648_e51bfdea5f55b401ada8_48.png",
        "image_72": "https://avatars.slack-edge.com/2020-03-23/1018755245648_e51bfdea5f55b401ada8_72.png"
      },
      "team_id": "TCXK1L988"
    },
    "blocks": [
      {
        "type": "section",
        "block_id": "w9Yk",
        "text": {
          "type": "mrkdwn",
          "text": "*F3999: <https://rally1.rallydev.com/#/detail/portfolioitem/feature/336974901388|Bezorgscan uit Salesforce naar LED>*",
          "verbatim": false
        }
      },
      {
        "type": "section",
        "block_id": "6I7h2",
        "fields": [
          {
            "type": "mrkdwn",
            "text": "*Status:* undefined",
            "verbatim": false
          },
          {
            "type": "mrkdwn",
            "text": "*Owner:* Dennis van  Steijn",
            "verbatim": false
          },
          {
            "type": "mrkdwn",
            "text": "*Last Update:* Mar 31, 2020, 7:18 AM",
            "verbatim": false
          },
          {
            "type": "mrkdwn",
            "text": "*Iteration:* -",
            "verbatim": false
          }
        ]
      }
    ],
    "channel": "CSGP9RTDJ",
    "event_ts": "1585743189.044700",
    "channel_type": "channel"
  },
  "type": "event_callback",
  "event_id": "Ev0116JS59HS",
  "event_time": 1585743189,
  "authed_users": [
    "U010LVAFUBG"
  ]
}

module.exports = {
  slack_url_verification, slack_event, slack_event_thread, slack_event_link, slack_event_bot, slack_event_hidden
}