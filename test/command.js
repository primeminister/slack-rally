"use strict";

process.env.NODE_ENV = "TEST";

const express = require("../built/server"),
  chai = require("chai"),
  chaiHttp = require("chai-http"),
  sinon = require("sinon"),
  expect = chai.expect,
  should = chai.should();

chai.use(chaiHttp);
chai.use(require("chai-as-promised"));

// mock slack post
let slack_cmd = {
  token: "yvuLIfnilv5BXtUSBC0HdXZm",
  team_id: "T026VMESC",
  team_domain: "mowd",
  channel_id: "CVC27GE3Y",
  channel_name: "rallytest",
  user_id: "U02LTE7JT",
  user_name: "primemininster",
  command: "/rally",
  text: "US42384",
  response_url: "https://hooks.slack.com/commands/T026VMESC/998400420903/O0ymfsDlSop3fbrsHV1Y6fMz",
  trigger_id: "996536811360.2233728896.3ffd51a121eb82be20d8ed653835ef17"
};

let server;

describe("Slack command /rally tests", function() {
  before("start http server", function(done) {
    express.startServer().then(function(srv) {
      server = srv;
      done();
    });
  });
  after("close http server", function(done) {
    express.closeServer(server).then(function(){
      done();
    });
  });
  afterEach("restore stubs", function () {
    sinon.restore();
  });
  it("404 to /", function testPath(done) {
    chai
      .request(express.app)
      .get("/")
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });
  it("404 everything else", function testPath(done) {
    chai
      .request(express.app)
      .get("/foo/bar")
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });
  it("respond to /rally help", function testPath(done) {
    sinon.stub(slack_cmd, 'text').value('help');
    chai
      .request(express.app)
      .get("/")
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });
  it("responds to /rally, UserStory", function testCmd(done) {
    sinon.stub(slack_cmd, 'text').value('US42384');
    chai
      .request(express.app)
      .post("/cmd")
      .send(slack_cmd)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(1);
        done();
      });
  });
  it("responds to /rally, Defect", function testCmd(done) {
    sinon.stub(slack_cmd, 'text').value('DE4099');
    chai
      .request(express.app)
      .post("/cmd")
      .send(slack_cmd)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(1);
        done();
      });
  });
  it("responds to /rally, TestSet", function testCmd(done) {
    sinon.stub(slack_cmd, 'text').value('TS45');
    chai
      .request(express.app)
      .post("/cmd")
      .send(slack_cmd)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(1);
        done();
      });
  });
  it("responds to /rally, Feature", function testCmd(done) {
    sinon.stub(slack_cmd, 'text').value('F4000');
    chai
      .request(express.app)
      .post("/cmd")
      .send(slack_cmd)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(1);
        done();
      });
  });
  it("responds to /rally with multiple issues", function testCmd(done) {
    sinon.stub(slack_cmd, 'text').value('Us42384, F4000, DE4099');
    chai
      .request(express.app)
      .post("/cmd")
      .send(slack_cmd)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.eql(3);
        done();
      });
  });
  it("responds to /rally with sprint {projectName}", function testCmd(done) {
    sinon.stub(slack_cmd, 'text').value('sprint LED DWH');
    chai
      .request(express.app)
      .post("/cmd")
      .send(slack_cmd)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.blocks.should.be.a("array");
        res.body.blocks.length.should.be.least(2);
        done();
      });
  });
});