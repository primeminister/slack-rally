"use strict"

const util = require("util");
const { createLogger, format, transports, config } = require("winston");
const { combine, timestamp, label, printf, errors } = format;
const expressWinston = require("express-winston");
const appConfig = require("config");

const myFormat = printf(({ level, message, timestamp }:any) => {
  timestamp = new Date(timestamp);
  if (typeof message === "object") {
    try {
      message = JSON.stringify(message, null, 2);
    } catch {
      message = util.format('%o', message);
    }
  }
  return `${timestamp.toISOString()} [${level}]: ${message}`;
});

// { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }

let logObj = {
  statusLevels: config.npm.levels,
  level: appConfig.Logger.level,
  format: format.combine(
    errors({ stack: true }),
    format.colorize(),
    timestamp(),
    format.splat(),
    myFormat
  ),
  transports: [new transports.Console()],
  winstonInstance: {},
  meta: true, // optional: control whether you want to log the meta data about the request (default to true)
  skip: function (req: any, res: any) {
    return false;
  },
};

if (process.env.NODE_ENV === "PROD") {
  logObj.transports = [
    new transports.File({
      filename: "log/error.log",
      level: "warn"
    }),
    new transports.File({
      filename: "log/combined.log"
    })
  ];
}
let logging = createLogger(logObj);

// set diff level for logging in express
logObj.winstonInstance = logging; // a winston logger instance. If this is provided the transports and formats options are ignored.
// log level to use, the default is "info".
// Assign a function to dynamically set the level based on request and response,
// or a string to statically set it always at that level.
// statusLevels must be false for this setting to be used.
logObj.statusLevels = false;
logObj.level = "info";
logObj.skip = function(_req, _res) {
  if (process.env.NODE_ENV === "PROD") {
    return true;
  }
  return false;
};
logging.express = expressWinston.logger(logObj);

// export so other modules can use them
export = {
  logger: logging
};
