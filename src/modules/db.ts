"use strict";
// Create a new instance of the WebClient class with the token read from your environment variable
const config = require("config"),
  { logger } = require("./logger");
import { Sequelize, Model, DataTypes, BuildOptions } from "sequelize";

const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: `${config.Database.path}/${config.Database.file}`,
  logging:
    logger.level === "silly" || logger.level === "debug"
      ? (msg) => logger.silly(msg)
      : false,
});

const Token = sequelize.define(
  "Token",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    slack_space: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    slack_token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    slack_admin: {
      type: DataTypes.STRING,
      // allowNull: false,
    },
    slack_domain: {
      type: DataTypes.STRING,
      // allowNull: false,
    },
    rally_api: {
      type: DataTypes.STRING,
      // allowNull: false,
    },
    rally_space: {
      type: DataTypes.INTEGER,
      // allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    updatedAt: {
      type: DataTypes.DATE,
    },
  },
  {
    tableName: "tokens",
    // options
    indexes: [
      {
        unique: true,
        fields: ["slack_space"],
      },
    ],
  }
);


/**
 * Interface for database row
 *
 * @export
 * @interface TokenRow
 */
export interface TokenRow {
  id?: number;
  slack_space: string;
  slack_token: string;
  slack_admin: string;
  slack_domain?: string;
  rally_api?: string;
  rally_space?: number;
}

/**
 * Checks database connection
 *
 * @export
 * @returns
 */
export function openDb() {
  return new Promise((resolve, reject) => {
    sequelize
      .authenticate()
      .then(() => {
        logger.info("Connection has been established successfully.");
        resolve();
      })
      .catch((err:any) => {
        reject(`Unable to connect to the database: ${err}`);
      });
  });
}

/**
 * Close database
 *
 * @export
 * @returns
 */
export function closeDb() {
  return sequelize.close();
}

/**
 * Get certain or all fields from db table tokens
 *
 * @export
 * @param {string} space
 * @param {string[]} [fields]
 * @returns
 */
export function read(space: string, fields?: string[]) {
  return new Promise(async (resolve, reject) => {
    if (!fields) {
      fields = ['*'];
    }
    // const dbFields:string = fields.join(',');
    let row = await sequelize.models.Token.findOne({
      attributes: fields,
      where: {
        slack_space: space,
      },
    });
    if (row) {
      resolve(row);
    } else {
      reject(`Did no find data with slack_space ${space}`);
    }
  });
}

/**
 * Read all rows from tokens table
 *
 * @export
 * @param {string[]} [fields]
 * @returns
 */
export function readAll(fields?: string[]) {
  return new Promise(async (resolve, reject) => {
    let row = await sequelize.models.Token.findAll({
      attributes: fields,
      order: [["id", "ASC"]],
    });
    if (row) {
      resolve(row);
    } else {
      reject(new Error('Did not find any records'));
    }
  });
}

/**
 * Store values in db table tokens
 *
 * @export
 * @param {string} space
 * @param {Token} data
 * @returns
 */
export function store(space: string, data: any) {
  return new Promise(async(resolve, reject) => {
    let [affect, t] = await sequelize.models.Token.update(data, {
      where: {
        slack_space: space,
      },
    });
    if (!affect) {
      sequelize.models.Token.create(data);
    }
    resolve();
  });
}