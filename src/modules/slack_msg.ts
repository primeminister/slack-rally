export const help_txt: string =
  'Retrieve details about Rally issues.\n' +
  '*Usage:* `/rally [USxxx,DExxx,TSTxxx,Fxxx|sprint {project}]`.\n' +
  '*Examples:* `/rally US56423`; `/rally US56423,DE9865,F4000`; `/rally sprint LED DWH`.';

export const modal_configure_rally: object = {
  view: {
    type: 'modal',
    title: {
      type: 'plain_text',
      text: 'Configure Rally app',
      emoji: true
    },
    submit: {
      type: 'plain_text',
      text: 'Submit',
      emoji: true
    },
    close: {
      type: 'plain_text',
      text: 'Cancel',
      emoji: true
    },
    blocks: [
      {
        type: 'input',
        block_id: 'rally_api',
        element: {
          type: 'plain_text_input',
          action_id: 'rally_api',
          placeholder: {
            type: 'plain_text',
            text: 'f.e. _zHjdh873hdi8hj3dih3dhw'
          }
        },
        label: {
          type: 'plain_text',
          text: 'Rally API key'
        }
      },
      {
        type: 'input',
        block_id: 'rally_space',
        element: {
          type: 'plain_text_input',
          action_id: 'rally_space',
          placeholder: {
            type: 'plain_text',
            text: 'f.e. 1231234827938'
          }
        },
        label: {
          type: 'plain_text',
          text: 'Rally Workspace ID'
        }
      }
    ]
  }
};

export const direct_msg_after_auth: object = {
  text: 'Rally app needs rally configuration',
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text:
          'Rally app needs your Rally configuration as in API key and workspace. You can find them in your <https://rally1.rallydev.com/login/accounts/index.html#/clients|Rally application manager>'
      }
    },
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: 'Click the button to open a dialog to enter your Rally data'
      },
      accessory: {
        type: 'button',
        text: {
          type: 'plain_text',
          text: 'Configure',
          emoji: false
        },
        value: 'configure_rally'
      }
    }
  ]
};

export const modal_create_rally_issue: object = {
  view: {
    type: 'modal',
    title: {
      type: 'plain_text',
      text: 'Create a Rally issue',
      emoji: true
    },
    submit: {
      type: 'plain_text',
      text: 'Submit',
      emoji: true
    },
    close: {
      type: 'plain_text',
      text: 'Cancel',
      emoji: true
    },
    blocks: [
      {
        type: 'input',
        label: {
          type: 'plain_text',
          text: 'What is the title of the issue?'
        },
        block_id: 'title',
        element: {
          type: 'plain_text_input',
          action_id: 'title',
          placeholder: {
            type: 'plain_text',
            text: 'f.e. SPIKE: how to handle performance tests'
          }
        }
      },
      {
        type: 'input',
        block_id: 'type',
        label: {
          type: 'plain_text',
          text: 'What is the type of the issue?',
          emoji: true
        },
        element: {
          type: 'static_select',
          action_id: 'type',
          placeholder: {
            type: 'plain_text',
            text: 'Select the issue type',
            emoji: true
          },
          options: [
            {
              text: {
                type: 'plain_text',
                text: 'Defect',
                emoji: true
              },
              value: 'defect'
            },
            {
              text: {
                type: 'plain_text',
                text: 'User Story',
                emoji: true
              },
              value: 'hierarchicalrequirement'
            },
            {
              text: {
                type: 'plain_text',
                text: 'Feature',
                emoji: true
              },
              value: 'portfolioitem'
            },
            {
              text: {
                type: 'plain_text',
                text: 'TestSet',
                emoji: true
              },
              value: 'testset'
            }
          ]
        }
      },
      {
        type: 'input',
        label: {
          type: 'plain_text',
          text: 'To which project does it belong to?'
        },
        block_id: 'project',
        element: {
          action_id: 'project',
          type: 'plain_text_input',
          placeholder: {
            type: 'plain_text',
            text: 'f.e. LED DWH'
          }
        }
      }
    ]
  }
};
