"use strict";

const rallyMod = require("rally"),
  queryUtils = rallyMod.util.query,
  refUtils = rallyMod.util.ref,
  config = require("config"),
  { logger } = require("./logger");
const RALLY_BASEURL: string = config.Rally.apiUrl;

let rallyAPI: any;
let RALLY_WORKSPACE_ID: number | undefined;

/**
 * Set rally module with headers and credentials
 *
 * @export
 * @param {*} teamData
 * @returns
 */
export function init(teamData: any) {
  if (!teamData || !teamData.rally_api || !teamData.rally_space) {
    logger.error(
      `Requested command but did not found rally credentials for teamID ${teamData.slack_space}`
    );
    return false;
  }

  rallyAPI = rallyMod({
    apiKey: teamData.rally_api, //preferred, required if no user/pass, defaults to process.env.RALLY_API_KEY
    apiVersion: "v2.0", //this is the default and may be omitted
    server: RALLY_BASEURL, //this is the default and may be omitted
    requestOptions: {
      headers: {
        "X-RallyIntegrationName": "Rally integration with Slack", //while optional, it is good practice to
        "X-RallyIntegrationVendor": "MoWD Slack-Rally Bot", //provide this header information
        "X-RallyIntegrationVersion": "1.0",
      },
    },
  });
  RALLY_WORKSPACE_ID = teamData.rally_space;
  return true;
}

/**
 * Get rally items based on the command from slack
 *
 * @export
 * @param {string} cmd
 * @param {string} projectName
 * @param {string} sItems
 * @returns
 */
export function getItems(cmd: string, projectName: string, sItems: string) {
  return new Promise((resolve, reject) => {
    logger.silly(`command = ${cmd}`);
    // Retrieve items in current sprint
    if (cmd === "sprint") {
      getSprintItems(projectName).then((items) => {
        resolve(items);
      });
    }
    // retrieve items based on the formattedID (US43319, DE12987, etc)
    else if (cmd === "ids") {
      getIds(sItems).then((items) => {
        resolve(items);
      });
    }
    // retrieve items based on the link
    else if (cmd === "links") {
      getFromLinks(sItems).then((items) => {
        resolve(items);
      });
    }
    else {
      reject(`Command not handled: ${cmd}`);
    }
  });
}

/**
 * Get Rally sprint items based on project name
 *
 * @param {string} projectName
 * @returns
 */
function getSprintItems(projectName: string) {
  return new Promise((resolve, reject) => {
    const d = convertDate();
    let rallyOptions = {
      query:
        '((Project.Name = "' +
        projectName +
        '") AND ((Iteration.StartDate <= "' +
        d +
        '") AND (Iteration.EndDate >= "' +
        d +
        '")))',
      pagesize: 50,
    };
    // do requests and only resolve after all is resolved
    let rallyResults = new Array();
    rallyResults.push(getStories(rallyOptions));
    rallyResults.push(getDefects(rallyOptions));
    let handleAllPromises = Promise.all(rallyResults);
    handleAllPromises
      .then((results: any[]) => {
        // concat results
        let items = new Array();
        results.forEach((result) => {
          items = items.concat(result.Results);
        });
        logger.debug("Got items inside getItems: " + items.length);
        resolve(items);
      })
      .catch((err: any) => reject(new Error(err)));
  });
}

/**
 * Get Rally items, based on string of issue numbers, seperated by comma
 *
 * @param {string} sItems
 * @returns
 */
function getIds(sItems: string) {
  return new Promise((resolve, reject) => {
    const reUS = RegExp("^US[0-9]{1,}"),
      reDE = RegExp("^DE[0-9]{1,}"),
      reTS = RegExp("^TS[0-9]{1,}"),
      reFT = RegExp("^F[0-9]{1,}");

    let rallyOptions = {};
    let formattedIDs: string[] = new Array();
    formattedIDs = sItems.split(",");

    // split items into arrays of types
    let features = new Array(),
      stories = new Array(),
      defects = new Array(),
      testsets = new Array();

    // loop and test against specific regex and add to designated array when matched
    formattedIDs.forEach((element) => {
      if (reFT.test(element)) {
        features.push(element);
      } else if (reUS.test(element)) {
        stories.push(element);
      } else if (reDE.test(element)) {
        defects.push(element);
      } else if (reTS.test(element)) {
        testsets.push(element);
      }
    });

    logger.debug(
      `rallyIDs found: ${features.join(",")};${stories.join(
        ","
      )};${defects.join(",")};${testsets.join(",")}`
    );

    let rallyPromises = new Array();
    if (features.length > 0) {
      rallyOptions = {
        limit: 5,
        query: queryUtils.where("FormattedID", "in", features.join(",")),
      };
      rallyPromises.push(getFeatures(rallyOptions));
    }
    if (stories.length > 0) {
      rallyOptions = {
        limit: 5,
        query: queryUtils.where("FormattedID", "in", stories.join(",")),
      };
      rallyPromises.push(getStories(rallyOptions));
    }
    if (defects.length > 0) {
      rallyOptions = {
        limit: 5,
        query: queryUtils.where("FormattedID", "in", defects.join(",")),
      };
      rallyPromises.push(getDefects(rallyOptions));
    }
    if (testsets.length > 0) {
      rallyOptions = {
        limit: 5,
        query: queryUtils.where("FormattedID", "in", testsets.join(",")),
      };
      rallyPromises.push(getTestSets(rallyOptions));
    }
    let handleAllPromises = Promise.all(rallyPromises);

    handleAllPromises
      .then((results: any[]) => {
        // concat results
        let items = new Array();
        results.forEach((result) => {
          items = items.concat(result.Results);
        });
        logger.info(`Number of items found: ${items.length}`);
        logger.silly(items);
        resolve(items);
      })
      .catch((err) => reject(new Error(err)));
  });
}

/**
 * Get items from links
 *
 * @param sItems string URLs of rally issues
 * @returns Rally items[]
 */
function getFromLinks(sItems: string) {
  return new Promise((resolve, reject) => {
    let links: string[] = sItems.split(','),
      rallyOptions: any = {limit: 1},
      rallyPromises = new Array();
    links.forEach(element => {
      let r = getRef(element).replace(/\/detail/gi, '');
      let type = getType(r);
      if (type === "feature" || type === "portfolioitem") {
        r = r.replace(/feature/gi, "portfolioitem");
      } else if (type === "userstory" || type === "hierarchicalrequirement") {
        r = r.replace(/userstory/gi, "hierarchicalrequirement");
      }
      rallyPromises.push(apiGet(r, rallyOptions));
    });
    let handleAllPromises = Promise.all(rallyPromises);
    handleAllPromises
      .then((results: any[]) => {
        // concat results
        let items = new Array();
        if (results.length > 0) {
          results.forEach((result) => {
            if (result.Object) {
              items.push(result.Object);
            }
          });
        }
        logger.info(`Number of items found: ${items.length}`);
        resolve(items);
      })
      .catch((err:any) => reject(new Error(err)));
  });
}


/**
 * Get all issues with type features (portfolio items)
 *
 * @param {*} rallyOptions
 * @returns
 */
function getFeatures(rallyOptions: any) {
  rallyOptions.type = "portfolioitem";
  return apiQuery(rallyOptions);
}

/**
 * Get all issues with type userstory (hierarchicalrequirement)
 *
 * @param {*} rallyOptions
 * @returns
 */
function getStories(rallyOptions: any) {
  rallyOptions.type = "hierarchicalrequirement";
  return apiQuery(rallyOptions);
}

/**
 * Get all issues with type Defect
 *
 * @param {*} rallyOptions
 * @returns
 */
function getDefects(rallyOptions: any) {
  rallyOptions.type = "defect";
  return apiQuery(rallyOptions);
}

/**
 *Get all issues with type TestSet
 *
 * @param {*} rallyOptions
 * @returns
 */
function getTestSets(rallyOptions: any) {
  rallyOptions.type = "testset";
  return apiQuery(rallyOptions);
}

/**
 * Get project Data
 *
 * @param {string} project
 * @returns
 */
function getProject(project: string) {
  if (!project) {
    return Promise.reject('Empty or no project param');
  }
  const rallyOptions = {
    type: 'project',
    query: '(Name contains "' + project + '")',
    fetch: ['ObjectID', 'Name'],
    order: 'Name',
    limit: 1
  };
  return apiQuery(rallyOptions);
}

/**
 * Cnvert data to current TZ
 *
 * @returns
 */
function convertDate() {
  return new Date().toISOString().slice(0, 10);
}

/**
 * Get type of an issue
 *
 * @export
 * @param {{ _ref: any }} item
 * @returns string  Type of issue
 */
export function getType(ref: string) {
  return refUtils.getType({ _ref: ref });
}

/**
 * Get reference of an issue
 *
 * @export
 * @param {{ _ref: any }} item
 * @returns
 */
export function getRef(ref: string) {
  return refUtils.getRelative({ _ref: ref });
}

/**
 * Create a link to the item that works in Rally
 *
 * @export
 * @param {{ObjectID: number; FormattedID: string; _ref: any;}} item
 * @returns
 */
export function createLink(item: {
  ObjectID: number;
  FormattedID: string;
  _ref: any;
}) {
  if (!item) {
    return "";
  }
  let type = getType(item._ref),
    objectID = item.ObjectID,
    formattedID = item.FormattedID;
  if (type === "hierarchicalrequirement") {
    type = "userstory";
  }
  return (
    "<" +
    RALLY_BASEURL +
    "/#/detail/" +
    type +
    "/" +
    objectID +
    " | " +
    formattedID +
    ">"
  );
}

/**
 * Creates a Rally issue
 *
 * @export
 * @param {string} title
 * @param {string} type
 * @param {string} project
 * @param {string} descr
 * @returns
 */
export function createItem(title: string, type: string, project: string) {
  return new Promise((resolve, reject) => {
    if (!title || !type || !project) {
      reject(new Error('Not enough data to create an issue'));
    }
    const data = {
      Name: title,
      project: project
    };
    logger.silly(type, data);
    // get project ID:
    getProject(project)
      .then((res: any) => {
        let result = res.Results[0];
        logger.debug(result);
        if (!result.ObjectID) {
          return reject(`Could not get project from name: ${data.project}`);
        }
        data.project = `/project/${result.ObjectID}`;
        // now create issue
        apiCreate(type, data)
          .then((result:any) => {
            logger.debug(result);
            resolve(result);
          }).catch((err:any) => {
            reject(err);
          });
      })
      .catch((err: any) => {
        reject(err);
      });
  });
}

/**
 * Update an issue
 *
 * @export
 * @param {string} objectRef
 * @param {{ ScheduleState: string }} data
 * @returns
 */
export function updateItem(objectRef: string, data: { ScheduleState: string }) {
  return new Promise((resolve, reject) => {
    if (!objectRef || !data.ScheduleState) {
      reject(new Error("No objectRef or new state provided"));
    }
    logger.debug(data);
    apiUpdate(objectRef, data)
      .then((result: unknown) => {
        resolve(result);
      })
      .catch((err: any) => reject(new Error(err)));
  });
}

/**
 * Set default rally options for API calls
 *
 * @param {*} options
 * @returns
 */
function setOptions(options: any) {
  let data: { [key: string]: string } = {};
  for (let key in options) {
    if (options.hasOwnProperty(key)) {
      if (typeof options[key] === "object") {
        data[key] = "/" + key + "?" + encodeURIComponent(options[key]);
      } else {
        data[key] = options[key];
      }
    }
  }
  if (options.fetch && typeof options.fetch === "string") {
    options.fetch = options.fetch.split(",");
  } else if (typeof options.fetch === "undefined") {
    options.fetch = [
      'type',
      'FormattedID',
      'Name',
      'PlanEstimate',
      'ScheduleState',
      'SubmittedBy',
      'ObjectID',
      'Owner',
      'LastUpdateDate',
      'Iteration',
      'PortfolioItem'
    ];
  }
  if (typeof options.order === "undefined") {
    options.order = "Rank";
  }
  options.start = 1;
  if (!options.limit) {
    options.limit = 50;
  }
  if (typeof options.scope === 'undefined') {
    options.scope = { workspace: "/workspace/" + RALLY_WORKSPACE_ID };
  } else if (!options.scope.workspace) {
    options.scope.workspace = "/workspace/" + RALLY_WORKSPACE_ID;
  }
  return options;
}

/**
 * Get rally issues based on ref
 *
 * @param ref string '/defect/1234'
 * @returns Promise
 */
function apiGet(ref: string, rallyOptions: any) {
  rallyOptions.ref = ref;
  rallyOptions = setOptions(rallyOptions);
  logger.silly(rallyOptions);
  return rallyAPI.get(rallyOptions);
}

/**
 * Request one or several issues
 *
 * @param {Object} rallyOptions
 * @returns
 */
function apiQuery(rallyOptions: any) {
  rallyOptions = setOptions(rallyOptions);
  logger.silly(rallyOptions);
  return rallyAPI.query(rallyOptions);
}

/**
 * actual request to the rally lib
 *
 * @param {string} type
 * @param {*} data
 * @returns
 */
function apiCreate(type:string, data: any) {
  let project:string;
  if (data.project) {
    project = data.project;
    delete data.project
  }
  let rallyOptions = {
    type: type,
    data: data,
    fetch: [
      'type',
      'FormattedID',
      'Name',
      'PlanEstimate',
      'ScheduleState',
      'SubmittedBy',
      'ObjectID',
      'Owner',
      'LastUpdateDate',
      'Iteration',
      'PortfolioItem'
    ],
    scope: {
      project: '',
      workspace: '/workspace/' + RALLY_WORKSPACE_ID
    }
  };
  if (project) {
    rallyOptions.scope.project = project;
  }
  logger.silly(rallyOptions);
  return rallyAPI.create(rallyOptions);
}

/**
 * actual request to the rally lib
 *
 * @param {string} objectRef
 * @param {*} data
 * @returns
 */
function apiUpdate(objectRef: string, data: any) {
  let rallyOptions = {
    ref: objectRef,
    data: data,
    fetch: [
      'type',
      'FormattedID',
      'Name',
      'PlanEstimate',
      'ScheduleState',
      'SubmittedBy',
      'ObjectID',
      'Owner',
      'LastUpdateDate',
      'Iteration',
      'PortfolioItem'
    ],
    scope: {
      workspace: '/workspace/' + RALLY_WORKSPACE_ID
    }
  };
  logger.silly(rallyOptions);
  return rallyAPI.update(rallyOptions);
}

