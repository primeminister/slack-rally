'use strict';

const crypto = require('crypto'),
  axios = require('axios').default,
  config = require('config'),
  { logger } = require('./logger'),
  Rally = require('./rally'),
  slack_msg = require('./slack_msg');

const SLACK_CLIENT_ID: string = process.env.SLACK_CLIENT_ID || '',
  SLACK_CLIENT_SECRET: string = process.env.SLACK_CLIENT_SECRET || '',
  SLACK_SIGNING_SECRET: string = process.env.SLACK_SIGNING_SECRET || '',
  SLACK_APIURL: string = config.Slack.apiUrl || 'https://slack.com/api';

// Setup Axios that does the get and post requests
const slackAxios = axios.create({
  baseURL: SLACK_APIURL
});
// Add a request interceptor
// slackAxios.interceptors.request.use(
//   function (axConfig: any) {
//     // Do something before request is sent
//     logger.silly(axConfig);
//     return axConfig;
//   },
//   function (err: any) {
//     logger.silly('(request) Axios Error: %j', err.config);
//     return Promise.reject(new Error(err));
//   }
// );
// // Add a response interceptor
// slackAxios.interceptors.response.use(
//   function (response: any) {
//     // Any status code that lie within the range of 2xx cause this function to trigger
//     // Do something with response data
//     logger.silly('(response) Resp data status 2xx: %j', response.data);
//     // logger.silly("(response) Resp status 2xx: %s", response.status);
//     // logger.silly("(response) Resp headers: %j", response.headers);
//     return response;
//   },
//   function (err: any) {
//     // Any status codes that falls outside the range of 2xx cause this function to trigger
//     // Do something with response err
//     if (err.response) {
//       // The request was made and the server responded with a status code
//       // that falls out of the range of 2xx
//       logger.silly('(response) Resp data: %j', err.response.data);
//       logger.silly('(response) Resp status != 2xx: %s', err.response.status);
//       logger.silly('(response) Resp headers: %j', err.response.headers);
//     } else if (err.request) {
//       // The request was made but no response was received
//       // `err.request` is an instance of XMLHttpRequest in the browser and an instance of
//       // http.ClientRequest in node.js
//       logger.silly('(response) Req error: %j', err.request);
//     } else {
//       // Something happened in setting up the request that triggered an err
//       logger.silly('(response) Generic error: %s', err.message);
//     }
//     logger.silly('(response) Axios Config: %j', err.config);
//     return Promise.reject(new Error(err));
//   }
// );

/**
 * Interface of SlackOptions
 *
 * @export
 * @interface SlackOptions
 */
export interface SlackOptions {
  channel_id?: string;
  response_url?: string | null;
  thread_ts?: string;
  teamid: string;
  token: string;
  admin?: string;
  trigger_id?: string;
  domain?: string;
  callback_id?: string;
}

/**
 * Validates that the request recieved is actually from Slack
 * https://gist.github.com/sachinr/838fefa9c5db42cef138c29d404d8bd2
 *
 * @export
 * @param {*} req
 * @returns
 */
export function verifyRequest(req: any) {
  if (process.env.NODE_ENV !== 'PROD' && process.env.NODE_ENV !== 'ACC') {
    return true;
  }
  const signature = req.headers['x-slack-signature'];
  const timestamp = req.headers['x-slack-request-timestamp'];

  if (Math.floor(new Date().valueOf() / 1000) - timestamp > 1000 * 60 * 5) {
    logger.error('x-slack-request-timestamp older than 5 minutes');
    return false;
  }

  const hmac = crypto.createHmac('sha256', SLACK_SIGNING_SECRET);
  const [version, hash] = signature.split('=');

  hmac.update(`${version}:${timestamp}:${req.rawBody}`);

  if (hmac.digest('hex') !== hash) {
    logger.error('x-slack-signature does not match');
    return false;
  }
  return true;
}

/**
 * Authenticae when using oAuth2
 *
 * @export
 * @param {*} req
 * @returns {object} message
 */
export function authenticate(req: any) {
  return new Promise(async (resolve, reject) => {
    // get permanent bot token with application/x-www-form-urlencoded format
    let params = new URLSearchParams();
    params.append('code', req.query.code);
    params.append('client_id', SLACK_CLIENT_ID);
    params.append('client_secret', SLACK_CLIENT_SECRET);
    let res = await slackAxios.post('/oauth.v2.access', params);
    if (res.status !== 200 || res.data.ok === false) {
      reject(new Error(res.data.error));
    }
    let teamData = res.data;
    // done, now the redirect
    res = await slackAxios.get(`/team.info?token=${teamData.access_token}&team=${teamData.team.id}`);
    if (res.status !== 200) {
      reject(new Error(res.data.error));
    } else {
      let domain: string = res.data.team.domain;
      resolve({
        slack_space: teamData.team.id,
        slack_token: teamData.access_token,
        slack_admin: teamData.authed_user.id,
        slack_domain: domain
      });
    }
  });
}

/**
 * Create single or mulitple messages for Slack
 *
 * @export
 * @param {string} cmd
 * @param {SlackOptions} options
 * @param {any[]} [items]
 * @returns
 */
export function createMessage(cmd: string, options: SlackOptions, items?: any[]) {
  let message: any = {};

  if (cmd === '' || cmd === 'help') {
    message.text = slack_msg.help_txt;
  } else if (cmd === 'block_actions') {
    if (items && items.length === 1) {
      let item = items[0];
      message.text = `Issue ${Rally.createLink(item)} is updated.`;
      message.replace_original = 'true';
    }
  } else if (items && items.length > 0) {
    message = createMessageRally(cmd, items);
  } else if (cmd === 'auth_configure') {
    message = slack_msg.direct_msg_after_auth;
  } else if (cmd === 'rally_configure') {
    message = slack_msg.modal_configure_rally;
  } else if (cmd === 'rally_create') {
    message = slack_msg.modal_create_rally_issue;
  } else {
    message.text = 'No items found for your command. Please try another command like `/rally help`';
  }
  // add mandetory fields to message
  if (message.text !== '' || message.blocks !== '') {
    // message.response_type = "ephemeral";
    message.channel = options.channel_id;
    if (options.thread_ts) {
      message.thread_ts = options.thread_ts;
    }
    if (options.trigger_id) {
      message.trigger_id = options.trigger_id;
    }
    if (options.callback_id) {
      message.callback_id = options.callback_id;
    }
  }
  return message;
}
/**
 * Creates a slack message based on multiple rally items
 *
 * @param {string} cmd
 * @param {any[]} items
 * @returns
 */
function createMessageRally(cmd: string, items: any[]) {
  let blocks = [];
  let obj: any = {};

  // if sprint requested then add block with sprintname
  if (cmd === 'sprint') {
    let item = items[0];
    const iteration = item.Iteration !== null ? item.Iteration.Name : '';
    const project = item.Project !== null ? item.Project._refObjectName : '';

    obj = {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `All issues in sprint *${iteration}* from project *${project}*`
      }
    };
    blocks.push(obj);
    obj = {
      type: 'divider'
    };
    blocks.push(obj);
  }

  // go through the items and show link plus short info
  for (let index = 0; index < items.length; index++) {
    const item = items[index];
    const objectID = item.ObjectID,
      formattedID = item.FormattedID,
      name = item.Name,
      iteration = item.Iteration ? item.Iteration.Name : '',
      project = item.Project ? item.Project._refObjectName : '',
      status = item.ScheduleState,
      owner = item.Owner ? item.Owner._refObjectName : '- No Owner -',
      blocked = item.Blocked,
      estimate = item.PlanEstimate ? item.PlanEstimate + 'pt' : '';
    let feature = '';
    if (item.PortfolioItem) {
      feature = Rally.createLink(item.PortfolioItem) + ': ' + item.PortfolioItem.Name;
    }
    let type = Rally.getType(item._ref);
    let accessory = {};
    // Adding a select box to change the status of an issue
    // Turned off, because users are not making use of this feature

    // if (type === 'hierarchicalrequirement' || type === 'defect') {
    //   accessory = addAccessory(item);
    // }
    let emoji = '';
    switch (status) {
      case 'Undefined':
        emoji = ':red_circle:';
        break;
      case 'Defined':
        emoji = ':white_circle:';
        break;
      case 'In-Progress':
        emoji = ':hourglass:';
        break;
      case 'Completed':
        emoji = ':ballot_box_with_check:';
        break;
      case 'Accepted':
        emoji = ':white_check_mark:';
        break;
      default:
        emoji = ':sunny:';
        break;
    }
    if (blocked) {
      emoji = ':bangbang:';
    }

    obj = {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text:
          '*' +
          Rally.createLink(item) +
          ': ' +
          name +
          '*\n' +
          emoji +
          ' _' +
          status +
          ' | ' +
          owner +
          (iteration ? ' | ' + iteration : '') +
          (project ? ' | ' + project : '') +
          (feature ? ' | ' + feature : '') +
          (estimate ? ' | ' + estimate : '') +
          '_'
      }
    };
    if (Object.keys(accessory).length > 0) {
      obj.accessory = accessory;
    }
    blocks.push(obj);
  }

  let msg = {
    blocks
  };
  return msg;
}

/**
 * Add accessory like dorpdown menu with new status
 *
 * @param {*} item Rally item
 * @returns
 */
function addAccessory(item: any) {
  let acc = {};
  let states = [
    { key: 'Undefined', val: 'Undefined' },
    { key: 'Defined', val: 'Defined' },
    { key: 'In-Progress', val: 'In Progress' },
    { key: 'Completed', val: 'Completed' },
    { key: 'Accepted', val: 'Accepted' }
  ];
  let options = [],
    type = Rally.getType(item._ref),
    objectRef = Rally.getRef(item._ref);
  for (let index = 0; index < states.length; index++) {
    const state = states[index];
    if (item.ScheduleState !== state.key) {
      options.push({
        text: {
          type: 'plain_text',
          text: state.val,
          emoji: false
        },
        value: objectRef + '|' + state.key
      });
    }
  }
  acc = {
    type: 'static_select',
    placeholder: {
      type: 'plain_text',
      text: 'Select a new state',
      emoji: false
    },
    options: options
  };
  return acc;
}

/**
 * Send slack message
 *
 * @export
 * @param {*} msg
 * @param {SlackOptions} options
 */
export function sendMessage(msg: any, options: SlackOptions) {
  if (!config.Slack.sendMsg) {
    logger.debug(options);
    logger.debug(msg);
  }
  else {
    if (!options.response_url || options.response_url === null) {
      options.response_url = '/chat.postMessage';
    }
    slackAxios
      .post(options.response_url, msg, {
        headers: { Authorization: 'Bearer ' + options.token }
      })
      .then((res: any) => {
        logger.debug(res);
      })
      .catch((err: any) => {
        logger.error(new Error(err.stack));
      });
  }
}

/**
 * Open direct message channel
 *
 * @export
 * @param {string} user
 * @param {SlackOptions} options
 * @returns
 */
export function getDirectMsgChannel(user: string, options: SlackOptions) {
  return new Promise(async (resolve, reject) => {
    let res = await slackAxios.post(
      '/conversations.open',
      { users: user },
      { headers: { Authorization: 'Bearer ' + options.token }}
    );
    if (res.data.channel && res.data.channel.id) {
      resolve(res.data.channel.id);
    } else {
      reject(new Error(res.data.error));
    }
  });
}

/**
 * Open a modal to let the user configure rally credentials
 *
 * @export
 * @param {object} msg
 * @param {SlackOptions} options
 * @returns
 */
export function openModal(msg: object, options: SlackOptions) {
  return new Promise(async (resolve, reject) => {
    let [res, err] = await doPostRequest('/views.open', msg, {
      headers: { Authorization: 'Bearer ' + options.token }
    });
    if (err !== null) {
      reject(err);
    }
    else if (res && res.data.ok !== true) {
      reject(JSON.stringify(res.data, null, 2));
    }
    else if (res) {
      resolve(res.data);
    }
    else {
      reject('Do not have any results from views.open modal');
    }
  });
}

/**
 * Wraper for slack posts
 *
 * @param {string} url
 * @param {*} data
 * @param {*} others
 * @returns
 */
async function doPostRequest(url:string, data:any, others:any) {
  try {
    let res = await slackAxios.post(url, data, others);
    return([res, null]);
  } catch (error) {
    return([null, error]);
  }
}
