'use strict';

//=======================================================
// Includes and Global Variables
//=======================================================
import express = require('express');
const bodyParser = require('body-parser'),
  config = require('config');

  // Local modules
const Slack = require('./modules/slack'),
  Rally = require('./modules/rally'),
  { logger } = require('./modules/logger'),
  db = require('./modules/db');
import type { TokenRow } from './modules/db';
import type { SlackOptions } from './modules/slack';

logger.info('NODE_ENV: ' + config.util.getEnv('NODE_ENV'));

const rawBodySaver = function (req: { rawBody: any }, res: any, buf: any, encoding: any) {
  if (buf && buf.length) {
    req.rawBody = buf.toString(encoding || 'utf8');
  }
};
const app: express.Application = express();

let APP_TEAM_DATA: TokenRow[] = [];

// Log our http layer
app.use(logger.express);
// Add raw body to new var, to use with authentication
app.use(bodyParser.urlencoded({ verify: rawBodySaver, extended: true }));
app.use(bodyParser.json({ verify: rawBodySaver }));
// and prevent CORS
app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
  res.setHeader('Access-Control-Allow-Origin', '*');

  if (req.method === 'OPTIONS') {
    res.sendStatus(500);
  } else {
    next();
  }
});

//=======================================================
// Handle web service calls
//=======================================================
/**
 * Handle /rally commands
 */
app.post('/cmd', (req, res) => {
  // verify request signature from slack
  if (!Slack.verifyRequest(req)) {
    return res.status(500).end();
  }
  // always send 200 when in prod/acc env
  if (process.env.NODE_ENV !== 'TEST') res.status(200).end();

  // --- check exceptions
  if (skipMessage('command', req.body)) {
    return;
  }

  // check if teamid has data
  let team_data: TokenRow | undefined;
  if (req.body.team_id) {
    team_data = APP_TEAM_DATA.find((o: any) => o.slack_space === req.body.team_id);
    if (!Rally.init(team_data)) {
      logger.error(`Could not init Rally for team: ${req.body.team_id}`);
      return;
    }
  }

  let options: SlackOptions = {
    channel_id: req.body.channel_id,
    response_url: req.body.response_url ? req.body.response_url : null,
    teamid: team_data ? team_data.slack_space : req.body.team_id,
    token: team_data ? team_data.slack_token : ''
  };

  if (req.body.text === 'help' || req.body.text === '') {
    let msg = Slack.createMessage('help', options);
    if (process.env.NODE_ENV === 'TEST') {
      res.status(200).send(msg);
    }
    Slack.sendMessage(msg, options);
    return;
  }

  // parse slack message
  let { cmd, projectName, formattedIDs } = parseRequest('commands', req.body.text);
  if (formattedIDs === null) {
    return;
  }
  // get rally items based on slack message
  Rally.getItems(cmd, projectName, formattedIDs)
    .then(function (items: any[]) {
      if (items.length > 0) {
        // create a reply message for slack
        let msg = Slack.createMessage(cmd, options, items);
        // send it back to slack
        if (process.env.NODE_ENV === 'TEST') {
          res.status(200).send(msg);
        }
        return Slack.sendMessage(msg, options);
      }
    })
    .catch(function (error: any) {
      logger.error(error.stack);
    });
});

/**
 * handle evenst, like listening to conversations and check for rally issues
 */
app.post('/events', function (req, res) {
  // logger.debug(req.body.event);

  // url verification handling
  if (req.body.type === 'url_verification') {
    return res.status(200).send(req.body.challenge);
  }

  // verify request signature from slack
  if (!Slack.verifyRequest(req)) {
    return res.status(500).end();
  }

  // always send 200 when in prod/acc env
  if (process.env.NODE_ENV !== 'TEST') res.status(200).end();

  // filter msg
  req.body.event.text = filterMsg(req.body.event.text);
  // check exceptions
  if (skipMessage('event', req.body)) {
    return;
  }

  // all good: lets analyze what has been said
  let { cmd, projectName, formattedIDs } = parseRequest(req.body.type, req.body.event.text);

  if (formattedIDs === null) {
    return;
  }

  // check if teamid has data
  let team_data: TokenRow | undefined;
  if (req.body.team_id) {
    team_data = APP_TEAM_DATA.find((o: any) => o.slack_space === req.body.team_id);
    if (!Rally.init(team_data)) {
      logger.error(`Could not init Rally for team: ${req.body.team_id}`);
      return;
    }
  }

  let options: SlackOptions = {
    channel_id: req.body.event.channel,
    response_url: req.body.response_url ? req.body.response_url : null,
    thread_ts: req.body.event.thread_ts ? req.body.event.thread_ts : req.body.event.ts,
    teamid: team_data ? team_data.slack_space : req.body.team_id,
    token: team_data ? team_data.slack_token : ''
  };

  Rally.getItems(cmd, null, formattedIDs)
    .then(function (items: any[]) {
      if (items.length > 0) {
        // create a reply message for slack
        let msg = Slack.createMessage(cmd, options, items);
        // send it back to slack
        if (process.env.NODE_ENV === 'TEST') {
          return res.status(200).send(msg);
        }
        Slack.sendMessage(msg, options);
      }
    })
    .catch(function (errors: any) {
      logger.error(errors.stack);
    });
});

/**
 * handle actions like update on issues and modal submissions
 */
app.post('/actions', (req, res) => {
  // verify request signature from slack
  if (!Slack.verifyRequest(req)) {
    return res.status(500).end();
  }
  if (!req.body.payload) {
    return res.status(200).end();
  }
  let payload = JSON.parse(req.body.payload);
  logger.debug('Payload: %j', payload);

  if (skipMessage(payload.type, payload)) {
    // check exceptions
    return;
  }

  // check if teamid has data
  let team_data: TokenRow | undefined;
  if (payload.team.id) {
    team_data = APP_TEAM_DATA.find((o: any) => o.slack_space === payload.team.id);
  }

  let options: SlackOptions = {
    channel_id: payload.channel ? payload.channel.id : '',
    response_url: payload.response_url ? payload.response_url : null,
    thread_ts: payload.container && payload.container.message_ts ? payload.container.message_ts : '',
    trigger_id: payload.trigger_id ? payload.trigger_id : '',
    teamid: team_data ? team_data.slack_space : payload.team.id,
    token: team_data ? team_data.slack_token : '',
    callback_id: payload.callback_id ? payload.callback_id : ''
  };
  let msg = {};

  // --- block actions: update status of an issue, configure rally ---------
  if (payload.type === 'block_actions') {
    // always send 200 when in prod/acc env
    if (process.env.NODE_ENV !== 'TEST') res.status(200).end();
    // handle updating rally item
    let msg = 'Cannnot update item, check errors',
      item:any[] = [];
    blockActions(payload, options, team_data)
      .then((result:any) => {
        if (result && result.Oject) {
          item = [result.Object];
          logger.debug('Rally item updated: %j', item);
          msg = Slack.createMessage('block_actions', options, item);
          Slack.sendMessage(msg, options);
        }
      })
      .catch((errors: any) => {
        logger.error('BlockActions error: %o', errors);
        if (process.env.NODE_ENV === 'TEST') res.status(500).end();
      });
  }
  // when clicking a shortcut, we nee dto open a modal
  else if (payload.type === 'message_action') {
    // always send 200 when in prod/acc env
    if (process.env.NODE_ENV !== 'TEST') res.status(200).end();
    shortcutActions(payload, options, team_data);
  }
  // its a submission from the modal
  else if (payload.type === 'view_submission') {
    if (!payload.view || !payload.view.state) {
      return res.status(404).end();
    }
    // now check if view_submission from modal should be
    // - save rally configuration
    // - create rally issue
    let viewState = payload.view.state;
    // create new Rally issue
    if (viewState.values.title) {
      createRallyIssue(payload, team_data)
        .then((result:any) => {
          let item = [result.Object];
          let msg = Slack.createMessage('ids', options, item);
          res.status(200).end();
          Slack.sendMessage(msg, options);
          // send message to confirm
        }).catch((err:any) => {
          logger.error('Error in creating rally item %o', err);
          res.status(200).end();
        });
    }
    // store rally credentials
    else if (viewState.values.rally_api) {
      saveConfigurationData(payload, team_data)
        .then((result: any) => {
          setTeamData();
          res.status(200).end();
        })
        .catch((err: any) => {
          logger.error('Error storing new credentials: %o', err);
          res.status(200).end();
        });
    }
    if (process.env.NODE_ENV === 'TEST') res.status(200).send(msg);
  }
  else {
    logger.info(`Not handling action : '${payload.type}'`);
    res.status(200).end();
  }
});

/**
 * handle oAuth and verification from slack calls
 */
app.get('/auth', function (req, res) {
  Slack.authenticate(req)
    .then((result: TokenRow) => {
      if (result === null) {
        return res.status(200).end();
      }
      processToken(result);
      // open a direct message channel to admin user
      let options: SlackOptions = {
        admin: result ? result.slack_admin : '',
        teamid: result ? result.slack_space : '',
        token: result ? result.slack_token : '',
        domain: result ? result.slack_domain : ''
      };
      return options;
    })
    .then((options: SlackOptions) => {
      if (options) {
        Slack.getDirectMsgChannel(options.admin, options).then((channel_id: string) => {
          if (channel_id) {
            options.channel_id = channel_id;
            let msg = Slack.createMessage('auth_configure', options);
            Slack.sendMessage(msg, options);
            if (process.env.NODE_ENV === 'TEST') {
              res.status(200).send(msg);
            }
          }
        });
        if (process.env.NODE_ENV !== 'TEST') {
          res.redirect('http://' + options.domain + '.slack.com');
        }
      }
    })
    .catch((err: any) => {
      logger.error('Error in Slack.auth/directmsg: %o', err);
      res.status(500).end();
    });
});

/**
 * Tests
 */
app.get('/test', function (req, res) {
  // Slack.processToken(req.query.space, req.query.token);
});

//-----------------------------------------------

/**
 * Handle actions on blocks:
 * - check if button 'configure rally' is clicked
 * - Update status of an issue
 *
 * @param {*} res
 * @param {*} payload
 * @param {SlackOptions} options
 * @param {(TokenRow | undefined)} team_data
 * @returns
 */
function blockActions(payload: any, options: SlackOptions, team_data: TokenRow | undefined) {
  // check if button 'configure rally' is clicked
  let action: string = payload.actions[0].value || '';
  logger.info('Action: %s', action);

  if (action === 'configure_rally') {
    options.response_url = null; // reset response url to use views.open for modal
    let msg = Slack.createMessage('rally_configure', options);
    Slack.openModal(msg, options);
    return Promise.resolve([]);
  }
  else {
    if (!Rally.init(team_data)) {
      return Promise.reject(`Could not init Rally for team: ${team_data.slack_space}`);
    }

    // action is a string with | for updating issue
    let action: string = payload.actions[0].selected_option.value,
      rallyItem: string[] = action.split('|'),
      objectRef: string = rallyItem[0];

    let data = { ScheduleState: '' };
    data.ScheduleState = rallyItem[1];

    // send both rally vars to rally to get the issue updated
    let msg = '',
      item:any[] = [];
    return Rally.updateItem(objectRef, data);
  }
}

/**
 * Handle actions coming from shortcuts, like creating rally issue
 *
 * @param {*} res
 * @param {*} payload
 * @param {SlackOptions} options
 * @param {(TokenRow | undefined)} team_data
 */
function shortcutActions(payload: any, options: SlackOptions, team_data: TokenRow | undefined) {
  // get callback_id
  let callback_id = options.callback_id;

  let msg = Slack.createMessage('rally_create', options);
  // if message text is preffilled, then)
  if (callback_id === 'create_msg' && payload.message.text) {
    logger.debug(`Message text: ${payload.message.text}`);
    msg.view.blocks[0].element.initial_value = payload.message.text;
  }
  Slack.openModal(msg, options).catch((err:any) => {
    logger.error(err);
  });
  return Promise.resolve();
}

/**
 * Check the fields and send values to Rally
 *
 * @param {*} payload
 * @param {(TokenRow | undefined)} team_data
 * @returns
 */
function createRallyIssue(payload: any, team_data: TokenRow | undefined) {
  return new Promise((resolve, reject) => {
    let viewState = payload.view.state,
      title = viewState.values.title.title.value,
      type = viewState.values.type.type.selected_option.value,
      project = viewState.values.project.project.value;

    if (!title) reject('title');
    if (!type) reject('type');
    if (!project) reject('project');

    if (!Rally.init(team_data)) {
      reject(`Could not init Rally for team: ${team_data.slack_space}`);
    }

    Rally.createItem(title, type, project)
      .then((result:any) => {
        resolve(result);
      }).catch((err:any) => {
        logger.error(`Could not create a rally issue: ${title};${type};${project}`);
        logger.error(err);
        reject(err);
      });
  });
}

/**
 * Save Rally configuration data that was submitted from the modal
 *
 * @param {*} res
 * @param {*} payload
 * @param {(TokenRow | undefined)} team_data
 * @returns
 */
function saveConfigurationData(payload: any, team_data: TokenRow | undefined) {
  let viewState = payload.view.state,
    rally_api = viewState.values.rally_api.rally_api.value,
    rally_space = viewState.values.rally_space.rally_space.value;
  if (!rally_api) {
    return Promise.reject('rally_api');
  } else if (!rally_space) {
    return Promise.reject('rally_space');
  }
  // update database with rally credentials
  let updateToken: TokenRow = {
    slack_space: team_data.slack_space,
    slack_token: team_data.slack_token,
    slack_admin: team_data.slack_admin,
    slack_domain: team_data.slack_domain,
    rally_api: rally_api,
    rally_space: rally_space
  };
  logger.debug('updateToken');
  logger.debug(updateToken);

  return db.store(team_data.slack_space, updateToken);
}

/**
 * Skip message under certain conditions
 *
 * @param {string} type
 * @param {{channel_id?: string; response_url?: string; text?: any; command?: string; type?: string; challenge?: string; event?: any; payload?: string; }} body
 * @returns
 */
function skipMessage(
  type: string,
  body: {
    channel_id?: string;
    response_url?: string;
    text?: any;
    command?: string;
    type?: string;
    challenge?: string;
    event?: any;
    payload?: string;
  }
) {
  let skip_msg = false;
  // Assume type events
  if (type === 'event' && body.type !== 'event_callback') {
    skip_msg = true;
  }
  if (type === 'event' && body.event.hidden && body.event.hidden === true) {
    skip_msg = true;
  }
  if (type === 'event' && body.event.subtype && body.event.subtype === 'message_deleted') {
    skip_msg = true;
  }
  // if msg is from our bot please ignore
  if (type === 'event' && body.event.bot_profile && body.event.bot_profile.name === 'Rally') {
    skip_msg = true;
  }
  // someone has to post something usefull
  if (type === 'command' && !body.text) {
    skip_msg = true;
  }
  if (type === 'event' && !body.event.text) {
    skip_msg = true;
  }
  if (type === 'event' && body.event.text !== '') {
    let txt = body.event.text;

    let regexEvent = / ((US|DE|TS|F)[0-9]{4,}) /gi;
    let matches = txt.match(regexEvent);
    if (matches === null || matches.length <= 0) {
      let regexEvent = /^((US|DE|TS|F)[0-9]{4,}) ?/gi;
      let matches = txt.match(regexEvent);
      if (matches === null || matches.length <= 0) {
        let regexEvent = / ((US|DE|TS|F)[0-9]{4,})/gi;
        let matches = txt.match(regexEvent);
        if (matches === null || matches.length <= 0) {
          skip_msg = true;
        }
      }
    }
  }
  // check if no formattedIds are found but still has rally links
  if (type === 'event' && body.event.text !== '' && skip_msg === true) {
    let filterRallyLinks: RegExp = new RegExp(`${config.Rally.apiUrl}`, 'gi');
    let txt = body.event.text;
    let check = txt.search(filterRallyLinks);
    if (check === -1) {
      skip_msg = true;
    } else {
      skip_msg = false;
    }
  }

  logger.info('Message skipped? ' + (skip_msg ? 'true' : false));
  return skip_msg;
}

/**
 * Filter stuff out of the message
 *
 * @param {string} msg
 * @returns
 */
function filterMsg(msg: string) {
  if (!msg) {
    return '';
  }
  logger.silly(`Msg BEFORE filter: ${msg}`);
  // only remove links from message other then rally links
  let filterRallyLinks: RegExp = new RegExp(`(<${config.Rally.apiUrl}[^>]+>)`, 'gi');
  // get the link, if rally link then do something with it
  let l: string[],
    links: string[] = [];
  while ((l = filterRallyLinks.exec(msg)) !== null) {
    links.push(l[1]);
  }
  // <https://www.url.nl|link text> or <http:www.url.nl>
  let filterLinks: RegExp = /<http(s)?:\/\/[^|>]+(|[^>]+)?>/gi;
  msg = msg.replace(filterLinks, '$2');
  // now paste back the rally links:
  msg = msg + ': ' + links.join(',');

  // filter users from message (<@UCY5WAF0C>)
  const filterUsers = /<@[^>]+>/gi;
  msg = msg.replace(filterUsers, '');
  // filter channel from message (<!rally_test>)
  const filterChannel = /<![^>]+>/gi;
  msg = msg.replace(filterChannel, '');
  // filter code blocks - multiline
  let filterCode = /```[^`]+```/gm;
  msg = msg.replace(filterCode, '');
  // - single line
  filterCode = /```.*/g;
  msg = msg.replace(filterCode, '');
  logger.silly(`Msg AFTER filter: ${msg}`);
  return msg;
}

/**
 * Parse the slack command text
 *
 * @param {string} cmd
 * @param {string} msg
 * @returns null in case of error or cmd:string, projectName:string, sItems:string
 */
function parseRequest(cmd: string, msg: string) {
  logger.debug(`Input parseRequest: ${cmd}, ${msg}`);
  // filter based on keyword
  let command: string = cmd,
    projectName: string = '',
    formattedIDs: string = '';

  const regexCmd = RegExp('^(US|DE|TS|F)[0-9]{1,8}');
  const regexSprint = RegExp('^sprint');
  const regexEvent = / ?((US|DE|TS|F)[0-9]{4,})/gi;
  let eventMatches = msg.match(regexEvent);
  // filter rally links, so we can also reply on that
  let filterRallyLinks: RegExp = new RegExp(`<(${config.Rally.apiUrl}[^>]+)>`, 'gi');
  // get the link, if rally link then do something with it
  let l: string[],
    links: string[] = [];
  while ((l = filterRallyLinks.exec(msg)) !== null) {
    let ll = l[1].replace(/\?.*$/, '');
    links.push(ll);
  }
  logger.debug(links);

  // check items
  if (regexCmd.test(msg) && cmd === 'commands') {
    command = 'ids';
    formattedIDs = msg;
  }
  // sprint
  else if (regexSprint.test(msg) && cmd === 'commands') {
    let msgArr = msg.split(' ');
    let temp = msgArr.shift();
    command = temp || '';
    projectName = msgArr.join(' ');
  }
  // events
  else if (eventMatches !== null && eventMatches.length > 0) {
    command = 'ids';
    let aIds = eventMatches.map(Function.prototype.call, String.prototype.trim);
    formattedIDs = aIds.join(',').toUpperCase();
  }
  else if (links.length > 0) {
    command = 'links';
    formattedIDs = links.join(',');
  }
  // reset to null instead of empty
  if (!formattedIDs) {
    formattedIDs = null;
  }
  logger.debug(`cmd: ${command}, pn: ${projectName}, ids: ${formattedIDs}`);
  return { cmd: command, projectName: projectName, formattedIDs: formattedIDs };
}

/**
 * get teamid's and token in memory
 *
 * @returns
 */
function setTeamData() {
  return new Promise(function (resolve, reject) {
    db.readAll()
      .then((rows: TokenRow[]) => {
        APP_TEAM_DATA = rows;
        logger.info(`TeamData has been set`);
        resolve(rows);
      })
      .catch((err: any) => {
        logger.error('setTeamData read error: %o', err);
        reject(new Error('No team data found'));
      });
  });
}

/**
 * Update new token in database
 *
 * @param {TokenRow} data
 */
function processToken(data: TokenRow) {
  // update in database
  db.store(data.slack_space, {
    slack_space: data.slack_space,
    slack_token: data.slack_token,
    slack_admin: data.slack_admin,
    slack_domain: data.slack_domain
  });
  // now update in memory table
  let updated: boolean = false;
  APP_TEAM_DATA.find((o: any, i: number) => {
    if (o.slack_space === data.slack_space) {
      APP_TEAM_DATA[i] = {
        slack_space: data.slack_space,
        slack_token: data.slack_token,
        slack_admin: data.slack_admin,
        slack_domain: data.slack_domain,
        rally_api: o.rally_api,
        rally_space: o.rally_space
      };
      updated = true;
      return true; // stop searching
    }
  });
  // if not found, then add to memory
  if (!updated) {
    APP_TEAM_DATA.push({
      slack_space: data.slack_space,
      slack_token: data.slack_token,
      slack_admin: data.slack_admin,
      slack_domain: data.slack_domain,
      rally_api: '',
      rally_space: 0
    });
  }
  logger.debug('APP_TEAM_DATA: %j', APP_TEAM_DATA);
}

//-----------------------------------------------
let server: any;
// Start our server
function startServer() {
  return new Promise((resolve, reject) => {
    db.openDb()
      .then(setTeamData)
      .then(() => {
        logger.silly('APP_TEAM_DATA: %j', APP_TEAM_DATA);
        let port = config.Server.port || 8083;
        server = app
          .listen(port, () => {
            logger.log('info', `App is listening on port ${port}`);
            if (process.send !== undefined) process.send('ready');
            resolve(server);
          })
          .on('error', (err: any) => {
            logger.error('Server error: %o', err);
            reject(err);
          });
      })
      .catch((err: any) => {
        logger.error(`Start Server + teamData error: ${err}`);
      });
  });
}
// this function closes the server, and returns a promise.
function closeServer(server: any) {
  return new Promise((resolve, reject) => {
    logger.log('info', 'Closing server');
    server.close((err: any) => {
      if (err) {
        return reject(new Error(err));
      }
      db.closeDb();
      resolve('ok');
    });
  });
}

// if server.js is called directly (aka, with `node server.js`), this block
// runs. but we also export the runServer command so other code (for instance, test code) can start the server as needed.
if (!module.parent) {
  startServer().catch((err) => logger.error('Error server module: %o', err));
}

process.on('SIGINT', function () {
  closeServer(server)
    .then(() => {
      process.exit(0);
    })
    .catch((err: any) => {
      logger.error(err);
      process.exit(1);
    });
});

//Export our modules for testing
export = { app, startServer, closeServer };
