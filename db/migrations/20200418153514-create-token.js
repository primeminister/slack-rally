'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('tokens', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      slack_space: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      slack_token: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      slack_admin: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      slack_domain: {
        type: Sequelize.STRING,
        allowNull: true
      },
      rally_api: {
        type: Sequelize.STRING,
        allowNull: true
      },
      rally_space: {
        type: Sequelize.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tokens');
  }
};