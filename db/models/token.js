'use strict';

import { Model, DataTypes } from "sequelize";
class Token extends Model {};
Token.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  },
  slack_space: {
    type: DataTypes.STRING,
    unique: true,
    allowNull: false,
  },
  slack_token: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  slack_admin: {
    type: DataTypes.STRING,
    // allowNull: false,
  },
  slack_domain: {
    type: DataTypes.STRING,
    // allowNull: false,
  },
  rally_api: {
    type: DataTypes.STRING,
    // allowNull: false,
  },
  rally_space: {
    type: DataTypes.INTEGER,
    // allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE,
  },
  updatedAt: {
    type: DataTypes.DATE,
  }
},
{
  tableName: "tokens",
  sequelize: sequelize,
  // options
  indexes: [{
    unique: true,
    fields: ['slack_space']
  }]
});
// @ts-ignore
Token.associate = function(models) {
  // associations can be defined here
};

// @ts-ignore
export default (sequelize, Model, DataTypes) => {

  return Token;
}