'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('tokens', [{
        slack_space: 'TEST001',
        slack_token: 'dummytokenxxx001',
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('tokens', null, {});
  }
};
